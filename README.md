# OpenML dataset: ANTM.JK-Stock-Market-2005-to-Jan-2021

https://www.openml.org/d/43470

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Overview
This dataset contains historical daily prices for all tickers currently trading on ANTM.JK. since 29th Sept 2005 until 3rd Feb 2021.
PT Aneka Tambang Tbk, colloquially known as Antam or ANTM.JK in Stock Market, is an Indonesian mining company. The company primarily produces gold and nickel, and is the largest producer of nickel in Indonesia.
The dataset was retrieved from Finance Yahoo - PT Aneka Tambang Tbk.
Background
There were news reported by stated that the increase in ANTM Stock was due to the impact of public sentiment on the battery holding industry. Moreover, the electric vehicle manufacturer from the United States, Tesla, plans to build a battery factory in Indonesia and the increasing nickel price and nickel ore sales volume in the domestic market are factors that influence ANTAM's stock trend.
In addition, the impact of influencers from Indonesia who have followed stock movements in the last few months has encouraged people to invest in various sectors, one of which is stock and that is ANTM's Stock.
source: CNN Indonesia and Bisnis.com
Data Structure
The column mean in dataset:
Date - specifies trading date
Open - the price at which a stock started trading when the opening bell rang
High - highest price at which a stock traded during a period
Low - lowest price at which a stock traded during a period
Close - the price of an individual stock when the stock exchange closed shop for the end of period
Adj Close - adjusted close price adjusted for both dividends and splits
Volume - the number of shares that changed hands during a given period

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43470) of an [OpenML dataset](https://www.openml.org/d/43470). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43470/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43470/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43470/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

